with Ada.Real_Time;
use Ada.Real_Time;

separate(Main)

task body Catalog is
   Catalog_Epoch : Time := Time_Last;
   Catalog_End : Time := Time_Last;
   Running : Boolean := True;
   Real_Captcha : Captcha;
begin
   Printer.Print(+"Catalog", +"Starting");
   accept Start(D : Duration) do
      Catalog_Epoch := Clock;
      Printer.Print(
		    +"Catalog",
		    +("Catalog started with duration"
			& Duration'Image(D)));
      Captcha_Generator.Create;
      Catalog_End := Catalog_Epoch + To_Time_Span(D);
   end Start;
   
   Captcha_Generator.Get(Real_Captcha);
   while Running loop
      select
	 accept Join(
		     ID : Student_Id;
		     P: Place;
		     C: Captcha;
		     Matched: out Boolean
		    ) do
	    Matched := Real_Captcha = C;
	    Printer.Print(
			  +"Catalog",
			  +("Got captcha attempt from " &
			      Student_Id'Image(ID) &
			      " who is " &
			      Place'Image(P)
			   )
			 );
	    if Matched then
	       Classroom.Add(ID);
	    end if;
	 end Join;
      or
	 delay until Catalog_End;
	 Running := False;
      end select;
   end loop;
   Printer.Print(+"Catalog", +"Catalog finished");
end Catalog;
