with Ada.Numerics.Discrete_Random;

separate(Main)

protected body Captcha_Generator is
   procedure Create Is
   begin
      for I in Current_Captcha'Range loop
	 Safe_Random.Random_Captcha_Character(Current_Captcha(I));
      end loop;
      Printer.Print(+"Captcha_Generator", +"Generated new captcha");
   end Create;
   
   procedure Get(C: out Captcha) is
   begin
      C := Current_Captcha;
   end Get;
   
   procedure Get_Wrong(C: out Captcha) is
   begin
      Get(C);
      if C(C'First) = Captcha_Character'First then
	 C(C'First) := Captcha_Character'Succ(C(C'First));
      else
	 C(C'First) := Captcha_Character'Pred(C(C'First));
      end if;
   end Get_Wrong;
end;
