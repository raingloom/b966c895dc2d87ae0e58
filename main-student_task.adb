with Ada.Numerics.Discrete_Random, Ada.Numerics.Float_Random;

separate(Main)

task body Student_Task is
   procedure Log(S : Unbounded_String) is
   begin
      Printer.Print(
		    +("Student "
			& To_String(Student_Names(Id)
				      & " "
				      & Place'Image(P))),
		    S
		   );
   end Log;
   
   subtype Percent is Integer range 1..100;
   
   package Percent_Random is
      new Ada.Numerics.Discrete_Random(
				       Result_Subtype => Percent
				      );
   use Percent_Random;
   
   G : Percent_Random.Generator;
   FG : Ada.Numerics.Float_Random.Generator;
   
   procedure Join(
		  Do_Succeed : Boolean;
		  Join_Ok : out Boolean
		 ) is
      C : Captcha;
   begin
      if Do_Succeed then
	 Captcha_Generator.Get(C);
	 Log(+"Trying right captcha");
      else
	 Captcha_Generator.Get_Wrong(C);
	 Log(+"Trying wrong captcha");
      end if;
      Catalog.Join(Id, P, C, Join_Ok);
      if Join_Ok then
	 Log(+"Captcha succeeded");
      end if;
   end Join;
   
   Captcha_Succeeded : Boolean := False;
begin
   Log(+"Started");
   while not Catalog'Terminated and not Captcha_Succeeded loop
      if P = On_Class then
	 Join(Random(G) >= 80, Captcha_Succeeded);
      else
	 Join(Random(G) >= 10, Captcha_Succeeded);
      end if;
   end loop;
   if Captcha_Succeeded then
      delay Duration(Ada.Numerics.Float_Random.Random(FG));
   end if;
   Log(+"Gonna leave class now");
   Classroom.Leave(Id);
   Log(+"Ended");
end Student_Task;
