with
  Ada.Text_IO, Ada.Real_Time,
  Ada.Containers.Ordered_Maps,
  Ada.Strings.Unbounded,
  Ada.Numerics.Discrete_Random;

use
  Ada.Text_IO,
  Ada.Real_Time,
  Ada.Strings.Unbounded;

procedure Main is
   subtype Student_Id is Natural range 1..10;
   subtype Captcha_Character is Character'Base range 'A'..'Z';
   type Captcha is array (Integer range 1..6) of Captcha_Character;
   type Left_Sheet is array (Student_Id) of Boolean;
   type Place is (At_Home, On_Class);
   type Attendance_Sheet is array (Student_Id) of Place;
   
   function "+" (Source : String) return Unbounded_String
     renames To_Unbounded_String;	
   
   Student_Names : array (Student_Id) of Unbounded_String :=
     (
      +"1",
      +"2",
      +"3",
      +"4",
      +"5",
      +"6",
      +"7",
      +"8",
      +"9",
      +"10"
     );
   
   Startup_Epoch : Ada.Real_Time.Time := Clock;
   
   protected Printer is
      procedure Print(T,S : Unbounded_String);
   end Printer;
   protected body Printer is separate;
   
   package Random_Student_P is new Ada.Numerics.Discrete_Random(
								Result_Subtype => Student_Id
							       );
   package Random_Captcha_Character_P is new Ada.Numerics.Discrete_Random(
									  Result_Subtype => Captcha_Character
									 );

   protected Safe_Random is
      procedure Initialize;
      entry Random_Captcha_Character(C : out Captcha_Character);
      entry Random_Student(S : out Student_Id);
   private
      Initialized : Boolean := False;
      SG : Random_Student_P.Generator;
      CG : Random_Captcha_Character_P.Generator;
   end Safe_Random;
   
   protected Captcha_Generator is
      procedure Create;
      procedure Get(C: out Captcha);
      procedure Get_Wrong(C: out Captcha);
   private
      Current_Captcha : Captcha;
   end Captcha_Generator;
   
   protected Door_Slam is
      procedure Signal;
      entry Wait;
   private
      Occured_Count : Natural := 0;
   end Door_Slam;
   
   task Catalog is
      entry Start(D : Duration);
      entry Join(
		 ID : Student_Id;
		 P: Place;
		 C: Captcha;
		 Matched: out Boolean
		);
   end Catalog;
   
   protected Classroom is
      procedure Add(Id : Student_Id);
      procedure Leave(Id : Student_Id);
      procedure Check_Random;
      procedure Print_Final;
   private
      Sheet : Attendance_Sheet := (others => At_Home);
      Left : Left_Sheet := (others => False);
   end Classroom;
   
   task type Student_Task(Id : Student_Id; P : Place);
   
   protected type Student_State is
      procedure Set_Place(P : Place);
      procedure Get_Place(P : out Place);
   private
      Plc : Place := At_Home;
   end Student_State;
   
   protected body Student_State is
      procedure Set_Place(P : Place) is
      begin
	 Plc := P;
      end Set_Place;
      
      procedure Get_Place(P : out Place) is
      begin
	 P := Plc;
      end Get_Place;
   end Student_State;
   
   type Student is record
      State : Student_State;
      Brain : access Student_Task;
   end record;
   
   procedure Init_Student(S : out Student; P : Place; I : Student_Id) is
   begin
      S.State.Set_Place(P);
      S.Brain := new Student_Task(I, P);
   end;
   
   Students : array (Student_Id) of Student;
   
   task Teacher;
   
   protected body Door_Slam is
      procedure Signal is
      begin
	 Occured_Count := Occured_Count + 1;
      end Signal;
      
      entry Wait when Occured_Count > 0 is
      begin
	 Occured_Count := Occured_Count - 1;
      end Wait;
   end Door_Slam;
   
   task body Teacher is separate;
   protected body Classroom is separate;
   task body Catalog is separate;
   protected body Captcha_Generator is separate;
   task body Student_Task is separate;
   protected body Safe_Random is separate;
begin
   Safe_Random.Initialize;
   
   for I in 1..5 loop
      Init_Student(Students(I), At_Home, I);
   end loop;
   
   for I in 6..10 loop
      Init_Student(Students(I), On_Class, I);
   end loop;
end Main;
