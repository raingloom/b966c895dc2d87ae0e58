separate(Main)

protected body Safe_Random is
   procedure Initialize is
   begin
      Random_Student_P.Reset(SG);
      Random_Captcha_Character_P.Reset(CG);
      Initialized := True;
      Printer.Print(+"Safe_Random",+"RNGs initialized");
   end Initialize;
   
   entry Random_Captcha_Character(C : out Captcha_Character) when Initialized is
   begin
      C := Random_Captcha_Character_P.Random(CG);
	Printer.Print(+"Safe_Random",+"Fulfilled random captcha character request");
   end Random_Captcha_Character;
   
   entry Random_Student(S : out Student_Id) when Initialized is
   begin
      S := Random_Student_P.Random(SG);
      Printer.Print(+"Safe_Random",+"Fulfilled random student request");   
   end Random_Student;
end Safe_Random;
