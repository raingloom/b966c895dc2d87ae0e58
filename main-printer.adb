with Ada.Text_IO, Ada.Real_Time;
use Ada.Text_IO, Ada.Real_Time;

separate(Main)

protected body Printer is
	procedure Print(T,S: Unbounded_String) is
	begin
		Put("[");
		Put(To_String(T));
		Put(" @ " & Duration'Image(To_Duration(Clock - Startup_Epoch)) & "] ");
		Put(To_String(S));
		New_Line;
	end Print;
end Printer;
