with Ada.Numerics.Discrete_Random;

separate(Main)

protected body Classroom is   
   procedure Add(Id : Student_Id) is
   begin
      Printer.Print(
		    +"Classroom", 
		    +("Adding "
			& Student_Id'Image(Id)
			& " to attendance sheet"));
      Sheet(Id) := On_Class;
   end Add;
   
   procedure Leave(Id : Student_Id) is
   begin
      Printer.Print(
		    +"Classroom",
		    +(Student_Id'Image(Id) & " left")
		   );
      Left(Id) := True; -- asume noboy returns
      Door_Slam.Signal;
      Printer.Print(+"Classroom", +"Door slammed");
   end Leave;
   
   procedure Check_Random is
      Student : Student_Id;
   begin
      Printer.Print(+"Classroom",+"Going to check a random student");
      Safe_Random.Random_Student(Student);
      Printer.Print(+"Classroom",+("Checking" & Student_Id'Image(Student)));
      if Left(Student) then
	 Printer.Print(+"Classroom", +(Student_Id'Image(Student) & " left, deleting them from the catalog"));
	 Sheet(Student) := At_Home;
      end if;
      Printer.Print(+"Classroom",+"Finished checking random student");
   end Check_Random;
   
   procedure Print_Final is
   begin
      for I in Sheet'Range loop
	 Printer.Print(
		       +"Classroom",
		       +(
			 To_String(Student_Names(I))
			   & " was "
			   & Place'Image(Sheet(I))
			)
		      );
      end loop;
   end Print_Final;
end Classroom;
