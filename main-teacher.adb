separate(Main)

task body Teacher is
   Teacher_Epoch : Time := Time_Last;
   Teacher_End : Time := Time_Last;
   Running : Boolean := True;
begin
   Teacher_Epoch := Clock;
   Teacher_End := Teacher_Epoch + To_Time_Span(15.0);
   Printer.Print(+"Teacher", +"Starting");
   Catalog.Start(5.0);
   Printer.Print(+"Teacher", +"Started catalog");
   while Running loop
      select
	 Door_Slam.Wait;
	 Printer.Print(+"Teacher", +"Notices someone left, starting random check");
	 Classroom.Check_Random;
	 Printer.Print(+"Teacher", +"Checked a random student");   
      or
	 delay until Teacher_End;
	 Printer.Print(+"Teacher", +"Class ended");   
	 Running := False;
      end select;
   end loop;
   Classroom.Print_Final;
   Printer.Print(+"Teacher", +"ending");
end Teacher;
